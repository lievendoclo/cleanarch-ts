import {
    CreateBuilding,
    CreateBuildingRequest,
    CreateBuildingResponse,
} from "cleanarch-ts-app-api";
import {
    Building,
    BuildingGateway,
} from "cleanarch-ts-domain";

export class CreateBuildingImpl implements CreateBuilding {
    private readonly buildingGateway: BuildingGateway;

    constructor(buildingGateway: BuildingGateway) {
        this.buildingGateway = buildingGateway;
    }

    public createBuilding(request: CreateBuildingRequest): CreateBuildingResponse {
        const building: Building = {
            id: "generated",
            name: request.name,
        };
        const created = this.buildingGateway.createBuilding(building);
        return {
            id: created.id,
            name: created.name,
        };
    }
}
