import * as express from "express";
import {
    CreateBuilding,
    CreateBuildingRequest,
} from "../../../app/api";

export function addBuildingEndpoints(
    expressApplication: express.Application,
    createBuilding: CreateBuilding,
): void {
    expressApplication.post("/building", (req, res) => {
        const response = createBuilding.createBuilding(req.body as CreateBuildingRequest);
        res.send(response);
    });
}
