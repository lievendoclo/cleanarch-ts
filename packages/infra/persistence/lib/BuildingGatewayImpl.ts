import { BuildingGateway } from "cleanarch-ts-domain";
import { Building } from "../../../domain";

export class BuildingGatewayImpl implements BuildingGateway {
    public createBuilding(building: Building): Building {
        return building;
    }
}