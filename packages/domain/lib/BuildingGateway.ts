import { Building } from "./Building";

export interface BuildingGateway {
    createBuilding(building: Building): Building;
}
