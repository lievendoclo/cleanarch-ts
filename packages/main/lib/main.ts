import {
    CreateBuildingImpl,
} from "cleanarch-ts-app-impl";
import * as web from "cleanarch-ts-infra-web";
import * as express from "express";
import { BuildingGateway } from "../../domain";
import { BuildingGatewayImpl } from "../../infra/persistence";

export function runApplication(): any {
    const app: express.Application = express();
    app.use(express.json())

    const buildingGateway: BuildingGateway = new BuildingGatewayImpl();
    const createBuilding = new CreateBuildingImpl(buildingGateway);

    app.listen(3000);

    web.addBuildingEndpoints(app, createBuilding);

}
